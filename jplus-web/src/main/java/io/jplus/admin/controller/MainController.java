package io.jplus.admin.controller;

import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.JplusConsts;
import io.jplus.admin.model.Config;
import io.jplus.admin.service.ConfigService;
import io.jplus.core.web.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresUser;

@RequestMapping(value = "/admin", viewPath = JplusConsts.BASE_VIEW_PATH)
public class MainController extends BaseController {

    @RPCInject
    ConfigService configService;

    @RequiresUser
    public void index() {
        Config config = configService.findConfigByKey("INDEX_PAGE");
        if (config != null) {
            render(config.getParamValue());
            return;
        }
        render("index.html");
    }

    public void main() {

        render("main.html");
    }

}
