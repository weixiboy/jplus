/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.controller;

import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jplus.JplusConsts;
import io.jplus.admin.model.Log;
import io.jplus.admin.service.LogService;
import io.jplus.core.web.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;


@RequestMapping(value = "/admin/log", viewPath = JplusConsts.BASE_VIEW_PATH + "admin")
public class LogController extends BaseController {

    @RPCInject
    LogService logService;


    public void index() {
        render("log.html");
    }

    @RequiresPermissions("sys:log:list")
    public void list() {
        Page<Log> page = logService.queryPage(getQuery());
        renderJsonPage(page);
    }

    @RequiresPermissions("sys:log:info")
    public void info() {
        String logId = getPara();
        Log log = logService.findById(logId);
        renderJson(Ret.ok("log", log));
    }


}
