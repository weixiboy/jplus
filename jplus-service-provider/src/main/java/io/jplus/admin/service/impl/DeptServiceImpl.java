package io.jplus.admin.service.impl;

import io.jboot.aop.annotation.Bean;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jplus.admin.model.Dept;
import io.jplus.admin.service.DeptService;
import io.jplus.common.Query;

import java.util.List;

@Bean
@RPCBean
public class DeptServiceImpl extends JbootServiceBase<Dept> implements DeptService {


    @Override
    public List<Dept> queryList(Query query) {
        Columns columns = Columns.create();
        List<Dept> deptList = DAO.findListByColumns(columns);
        for (Dept dept : deptList) {
            Dept parent = findById(dept.getPid());
            if (parent != null) {
                dept.setPname(parent.getName());
            }
        }
        return deptList;
    }

    @Override
    public Dept findDeptById(Object id) {
        Dept dept = findById(id);
        Dept parent = findById(dept.getPid());
        if (parent != null) {
            dept.setPname(parent.getName());
        }
        return dept;
    }

    @Override
    public boolean deleteById(Object... ids) {
        boolean tag = false;
        for (Object id : ids) {
            tag = DAO.deleteById(id);
        }
        return tag;
    }
}