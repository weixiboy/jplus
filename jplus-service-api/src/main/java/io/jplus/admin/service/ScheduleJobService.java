package io.jplus.admin.service;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.db.model.Columns;
import io.jplus.admin.model.ScheduleJob;

import java.util.List;

public interface ScheduleJobService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public ScheduleJob findById(Object id);


    /**
     * find all model
     *
     * @return all <ScheduleJob
     */
    public List<ScheduleJob> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(ScheduleJob model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(ScheduleJob model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(ScheduleJob model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(ScheduleJob model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<ScheduleJob> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<ScheduleJob> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<ScheduleJob> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);


}